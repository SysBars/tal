SysBars
~~~~~~~

(c) 2008 Tony Houghton
h@realh.co.uk
http://www.realh.co.uk

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Introduction
------------

SysBars is a ROX applet, written in python, to provide a simple and compact
view of system resource usage with animated bars on the panel. It currently has
readouts for CPU, RAM and swap usage and disk and network throughput.
Unfortunately the sources of this information vary from platform to platform
and currently only Linux is supported, probably requiring a fairly recent 2.6
series kernel, but SysBars is designed to be extensible by adding "plugins" for
new types of resource to monitor and "platform" extensions to support systems
other than Linux.

Locations
---------

SysBars' home page is <http://www.realh.ukfsn.org/rox.shtml#sysbars>. It has a
git repository at <http://repo.or.cz/w/SysBars.git>. For a ChangeLog see
<http://repo.or.cz/w/SysBars.git?a=log>.

Plugins
-------

To add a new plugin, just add a python file to the plugins directory. It's
easiest to copy one of the existing plugins, and also check each of the files
it imports for comments on how to derive from the base classes.

Generally a plugin will have to use some platform-specific code and you will
need to edit each platform file to provide any services you need.

Platforms
---------

Each platform file (belonging in the "platform" directory, surprisingly enough)
must be named after the platform as reported by python's sys.platform variable
(NB Linux is called "linux2" because of the 2.x kernel) with a .py suffix, and
provide the functions needed by each plugin. See the "public API" section at
the end of the linux2 file.

