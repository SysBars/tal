from rox import g

from choices import settings

HSPACING = 4
VSPACING = 4

class Dialog(g.Dialog):
	""" Base dialog for adding or editing a bar. """
	def __init__(self, parent_win, add, desc, bar_w = None, label_w = None):
		self.bar_w = bar_w
		self.client_label = label_w
		self.parent_win = parent_win
		if add:
			title = _("New %s bar") % self.get_type_name()
			buttons = (g.STOCK_CANCEL, g.RESPONSE_CANCEL,
				g.STOCK_OK, g.RESPONSE_OK)
			self.instant_apply = False
		else:
			title = _("Edit %s bar") % self.get_type_name()
			buttons = (g.STOCK_CLOSE, g.RESPONSE_CLOSE)
			self.instant_apply = True
		g.Dialog.__init__(self, title, parent_win,
			g.DIALOG_MODAL | g.DIALOG_DESTROY_WITH_PARENT, buttons)
		self.desc = desc
		self.label_size_group = g.SizeGroup(g.SIZE_GROUP_HORIZONTAL)
		self.set_default_response(g.RESPONSE_OK)
		self.layout()
	
	def get_type_name(self):
		return _("?")
	
	def make_label(self, label):
		l = g.Label(label)
		l.set_alignment(1, 0.5)
		self.label_size_group.add_widget(l)
		return l
	
	def layout(self):
		hbox = g.HBox(False)
		
		hbox.pack_start(self.make_label(_("Label")) , False, True, HSPACING)
		self.label_w = g.Entry()
		if self.instant_apply:
			l = self.desc.label
		else:
			l = self.get_default_label()
		self.label_w.set_text(l)
		self.label_w.set_activates_default(True)
		hbox.pack_start(self.label_w, True, True, HSPACING)
		self.vbox.pack_start(hbox, True, True, VSPACING)
		
		hbox = g.HBox(False)
		hbox.pack_start(self.make_label(_("Colour")) , False, True, HSPACING)
		self.colour_w = g.ColorButton(self.desc.colour)
		hbox.pack_start(self.colour_w, True, True, HSPACING)
		self.vbox.pack_start(hbox, True, True, VSPACING)
		
		hbox = g.HBox(False)
		hbox.pack_start(self.make_label(_("Width")) , False, True, HSPACING)
		self.width_w = g.SpinButton()
		self.width_w.set_range(1, 640)
		self.width_w.set_increments(1, 10)
		self.width_w.set_value(self.desc.width)
		self.width_w.set_activates_default(True)
		hbox.pack_start(self.width_w, True, True, HSPACING)
		self.vbox.pack_start(hbox, True, True, VSPACING)
		
		hbox = g.HBox(False)
		hbox.pack_start(self.make_label(_("Update every")) , False, True,
			HSPACING)
		self.min_period_w = g.SpinButton(digits = 1)
		self.min_period_w.set_range(0.1, 10)
		self.min_period_w.set_increments(0.1, 1)
		self.min_period_w.set_value(self.desc.min_period)
		self.min_period_w.set_activates_default(True)
		hbox.pack_start(self.min_period_w, True, True, HSPACING)
		hbox.pack_start(g.Label(_("to")) , False, False, HSPACING)
		self.max_period_w = g.SpinButton(digits = 1)
		self.max_period_w.set_range(0.1, 10)
		self.max_period_w.set_increments(0.1, 1)
		self.max_period_w.set_value(self.desc.max_period)
		self.max_period_w.set_activates_default(True)
		hbox.pack_start(self.max_period_w, True, True, HSPACING)
		hbox.pack_start(g.Label(_("secs")) , False, False, HSPACING)
		self.vbox.pack_start(hbox, True, True, VSPACING)
		
		if self.instant_apply:
			self.label_w.connect("changed", self.label_changed_cb)
			self.colour_w.connect("color-set", self.colour_set_cb)
			self.width_w.connect("value-changed", self.width_changed_cb)
			self.min_period_w.connect("value-changed",
				self.min_period_changed_cb)
			self.max_period_w.connect("value-changed",
				self.max_period_changed_cb)
		
		self.layout_for_type()
	
	def run_apply_destroy(self):
		self.show_all()
		response = self.run()
		if response == g.RESPONSE_OK:
			self.apply()
		self.destroy()
		return response
	
	def get_default_label(self):
		""" Override in derived classes """
		return _("?")
		
	def layout_for_type(self):
		""" Override in derived classes """
		pass
	
	def apply(self):
		""" Override to read additional settings for specific bar type;
		don't forget to call this base method too. """
		self.label_changed_cb()
		self.colour_set_cb()
		self.width_changed_cb()
		self.min_period_changed_cb()
		self.max_period_changed_cb()
	
	def label_changed_cb(self, w = None):
		self.desc.label = self.label_w.get_text()
		settings.set_bar_label(self.desc.num, self.desc.label)
		if self.instant_apply:
			self.client_label.set_text(self.desc.label)
		
	def colour_set_cb(self, w = None):					
		self.desc.colour = self.colour_w.get_color()
		settings.set_bar_colour(self.desc.num, self.desc.colour)
		if self.instant_apply:
			self.bar_w.change_colour(self.desc.colour)
		
	def width_changed_cb(self, w = None):					
		self.desc.width = self.width_w.get_value_as_int()
		settings.set_bar_width(self.desc.num, self.desc.width)
		if self.instant_apply:
			self.bar_w.change_width(self.desc.width)
		
	def min_period_changed_cb(self, w = None):
		self.desc.min_period = self.min_period_w.get_value()
		settings.set_bar_min_period(self.desc.num, self.desc.min_period)
		# Don't change bar now, just let it get updated next tick
		
	def max_period_changed_cb(self, w = None):
		self.desc.max_period = self.max_period_w.get_value()
		settings.set_bar_max_period(self.desc.num, self.desc.max_period)
		# Don't change bar now, just let it get updated next tick
		

