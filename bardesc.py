from choices import settings

class Description:
	""" Base class to be overridden by plugin-specific Descriptions """
	def __init__(self, num):
		self.num = num
		self.label = settings.get_bar_label(num)
		self.colour = settings.get_bar_colour(num)
		self.width = settings.get_bar_width(num)
		self.min_period = settings.get_bar_min_period(num)
		self.max_period = settings.get_bar_max_period(num)
	
	def change_num(self, num):
		""" Overriden versions in plugins should also call this. """
		self.num = num
		settings.set_bar_type(num, self.type)
		settings.set_bar_label(num, self.label)
		settings.set_bar_colour(num, self.colour)
		settings.set_bar_width(num, self.width)
		settings.set_bar_min_period(num, self.min_period)
		settings.set_bar_max_period(num, self.max_period)
		
	def get_name(self):
		return _("?")
	
	def get_range(self):
		return (0, 100)
	
	def get_reading(self):
		return 0

