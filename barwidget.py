import gobject
from rox import g

BORDER = 1

# Orientation refers to orientation of bar, which is perpendicular to panel
HORIZ = g.ORIENTATION_HORIZONTAL
VERT  = g.ORIENTATION_VERTICAL

class BarWidget(g.DrawingArea):
	def __init__(self, desc, orient, length = -1):
		self.desc = desc
		self.orient = orient
		self.length = length
		self.total_width = desc.width + 2 * BORDER
		self.period = desc.min_period
		self.reading = 0	# reading is scaled to window coords
		self.gc = None
		self.quiet = False
		
		g.DrawingArea.__init__(self)
		
		self.connect("configure-event", self.configure_event_cb)
		self.connect("expose-event", self.expose_event_cb)
		
		self.change_width(desc.width)
			
		gobject.timeout_add(int(self.period * 1000), self.tick_cb)
	
	def configure_event_cb(self, widget, event):
		if self.orient == HORIZ:
			self.length = event.width
			self.total_width = event.height
		else:
			self.length = event.height
			self.total_width = event.width
	
	def scale_reading(self, reading):
		min, max = self.desc.get_range()
		if reading < min:
			reading = min
		if reading > max:
			reading = max
		return int(self.length * (reading - min) / (max - min))
	
	def bar_edge(self):
		return int((self.total_width - self.desc.width) / 2)
	
	def mark_for_redraw(self, min_scaled, max_scaled):
		if self.orient == HORIZ:
			self.queue_draw_area(min_scaled, self.bar_edge(),
				max_scaled - min_scaled, self.desc.width)
		else:
			self.queue_draw_area(self.bar_edge(), self.length - max_scaled,
				self.desc.width, max_scaled - min_scaled)
			
	def tick_cb(self):
		try:
			reading = self.scale_reading(self.desc.get_reading())
		except:
			if not self.quiet:
				self.quiet = True
				# Hopefully this should be equivalent to returning False
				# so we won't get any more ticks for this gauge, but use
				# quiet flag in case
				raise
			reading = 0
		if reading == self.reading:
			period = self.period * 2
			if period > self.desc.max_period:
				period = self.desc.max_period
		else:
			if self.window and self.length > 0:
				min_scaled = min(reading, self.reading)
				max_scaled = max(reading, self.reading)
				self.mark_for_redraw(min_scaled, max_scaled)
			self.reading = reading
			period = self.desc.min_period
		if self.quiet:
			return False
		if period == self.period:
			return True
		else:
			self.period = period
			gobject.timeout_add(int(period * 1000), self.tick_cb)
			return False
			
	def expose_event_cb(self, widget, event):
		if self.orient == HORIZ:
			filled_rect = g.gdk.Rectangle(0, self.bar_edge(),
				self.reading, self.desc.width)
		else:
			filled_rect = g.gdk.Rectangle(self.bar_edge(),
				self.length - self.reading,
				self.desc.width, self.reading)
		filled_rect = filled_rect.intersect(event.area)
		if not self.gc:
			self.gc = self.window.new_gc()
			self.gc.copy(self.get_style().fg_gc[g.STATE_NORMAL])
			self.gc.set_rgb_fg_color(self.desc.colour)
		self.window.draw_rectangle(self.gc, True, *filled_rect)
		return False
	
	def change_colour(self, colour):
		if self.gc:
			self.gc.set_rgb_fg_color(colour)
			self.whole_redraw()
		
	def change_width(self, width):
		if self.orient == HORIZ:
			self.set_size_request(self.length, width + 2 * BORDER)
		else:
			self.set_size_request(width + 2 * BORDER, self.length)
	
	def whole_redraw(self):
		self.mark_for_redraw(0, self.length)
			
		
		