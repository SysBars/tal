import ConfigParser
import os.path

from rox import basedir, g

def colour_to_string(c):
	if hasattr(c, "to_string"):
		return c.to_string()
	else:
		return "#%04x%04x%04x" % (c.red, c.green, c.blue)


class Settings(ConfigParser.ConfigParser):
	def __init__(self):
		ConfigParser.ConfigParser.__init__(self)
		f = basedir.load_first_config('realh.co.uk', 'SysBars', 'options')
		if f:
			self.read(f)
		if not self.has_section('DEFAULT'):
			self.add_section('DEFAULT')
	
	def save(self):
		f = os.path.join(basedir.save_config_path('realh.co.uk', 'SysBars'),
			'options')
		fp = open(f, 'w')
		self.write(fp)
		fp.close()
	
	def set(self, section, option, value):
		ConfigParser.ConfigParser.set(self, section, option, str(value))
		self.save()
	
	def set_general(self, option, value):
		self.set('DEFAULT', option, value)
	
	def set_bar(self, bar, option, value):
		""" bar here is a number """
		sec = 'bar%d' % bar
		if not self.has_section(sec):
			self.add_section(sec)
		self.set(sec, option, value)
	
	def get_bar(self, bar, option, value = None):
		""" bar here is a number; value is default """
		try:
			return self.get('bar%d' % bar, option)
		except:
			return value
	
	def get_bar_int(self, bar, option, value):
		""" bar here is a number; value is default """
		try:
			return self.getint('bar%d' % bar, option)
		except:
			return value
	
	def get_bar_float(self, bar, option, value):
		""" bar here is a number; value is default """
		try:
			return self.getfloat('bar%d' % bar, option)
		except:
			return value
	
	def get_label_font(self):
		try:
			return self.get('DEFAULT', 'font')
		except:
			return 'Sans 8'

	def set_label_font(self, font):
		self.set('DEFAULT', 'font', font)
	
	def get_label_colour(self):
		try:
			cname = self.get('DEFAULT', 'label_colour')
			return g.gdk.color_parse(cname)
		except:
			return g.gdk.Color()
		
	def set_label_colour(self, gcolour):
		self.set('DEFAULT', 'label_colour', colour_to_string(gcolour))
	
	def get_num_bars(self):
		try:
			return self.getint('DEFAULT', 'nbars')
		except:
			return 1
			
	def set_num_bars(self, value):
		self.set('DEFAULT', 'nbars', str(value))
		
	def get_bar_type(self, bar):
		return self.get_bar(bar, 'type', 'cpu')

	def set_bar_type(self, bar, value):
		self.set_bar(bar, 'type', str(value))

	def get_bar_label(self, bar):
		return self.get_bar(bar, 'label', _("C"))

	def set_bar_label(self, bar, value):
		self.set_bar(bar, 'label', value)

	def get_bar_colour(self, bar):
		try:
			return g.gdk.color_parse(self.get_bar(bar, 'colour', '#206020'))
		except:
			return g.gdk.Color(0x4040, 0x4040, 0x4040)

	def set_bar_colour(self, bar, gcolour):
		self.set_bar(bar, 'colour', colour_to_string(gcolour))

	def get_bar_width(self, bar):
		return self.get_bar_int(bar, 'width', 16)

	def set_bar_width(self, bar, value):
		self.set_bar(bar, 'width', str(value))

	def get_bar_min_period(self, bar):
		return self.get_bar_float(bar, 'min_period', 0.2)

	def set_bar_min_period(self, bar, value):
		 self.set_bar(bar, 'min_period', value)

	def get_bar_max_period(self, bar):
		return self.get_bar_float(bar, 'max_period', 1)

	def set_bar_max_period(self, bar, value):
		self.set_bar(bar, 'max_period', value)
	
	def get_throughput(self, device, default = 0):
		try:
			return self.getint('throughput', device)
		except:
			return default
			
	def set_throughput(self, device, value):
		if not self.has_section('throughput'):
			self.add_section('throughput')
		self.set('throughput', device, value)



settings = Settings()
		
	
