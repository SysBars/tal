import gobject
from rox import g

from choices import settings
import plugins

VSPACING = 6
HSPACING = 6

class Dialog(g.Dialog):
	def __init__(self, client, parent = None):
		self.client = client
		g.Dialog.__init__(self, _("SysBars Choices"), parent,
			buttons = (g.STOCK_CLOSE, g.RESPONSE_CLOSE))
		self.set_has_separator(False)
		
		#table = g.Table(2, 2)
		#table.set_row_spacings(VSPACING)
		
		hbox = g.HBox(False, 0)
		l = g.Label(_("Label font"))
		#l.set_alignment(0, 0.5)
		hbox.pack_start(l, False, True, HSPACING)
		#table.attach(l, 0, 1, 0, 1, xoptions = g.FILL, xpadding = HSPACING)
		self.font_w = g.FontButton(settings.get_label_font())
		self.font_w.set_title(_("SysBar Label Font"))
		self.font_w.connect("font-set", self.font_set_cb)
		hbox.pack_start(self.font_w, True, True, HSPACING)
		#table.attach(self.font_w, 1, 2, 0, 1, xpadding = HSPACING)
		#l = g.Label(_("Label colour"))
		#l.set_alignment(0, 0.5)
		#table.attach(l, 0, 1, 1, 2, xoptions = g.FILL, xpadding = HSPACING)
		#self.label_colour_w = g.ColorButton(settings.get_label_colour())
		#self.label_colour_w.connect("color-set", self.label_colour_set_cb)
		#table.attach(self.label_colour_w, 1, 2, 1, 2, xpadding = HSPACING)
		#self.vbox.pack_start(table, padding = VSPACING)
		self.vbox.pack_start(hbox, padding = VSPACING)
		
		frame = g.Frame()
		l = g.Label(_("<b>System Monitor Bars</b>"))
		l.set_use_markup(True)
		frame.set_label_widget(l)
		frame.set_shadow_type(g.SHADOW_NONE)
		self.vbox.pack_start(frame, padding = VSPACING)
		
		self.list_store = g.ListStore(gobject.TYPE_STRING,
			gobject.TYPE_PYOBJECT)
		for b in plugins.all_descs:
			iter = self.list_store.append()
			self.list_store.set(iter, 0, b.get_name(), 1, b)
		
		self.tree_view = g.TreeView(self.list_store)
		self.tree_view.append_column( \
			g.TreeViewColumn(None, g.CellRendererText(), text = 0))
		self.tree_view.set_headers_visible(False)
		self.tree_view.connect("row-activated", self.row_activated_cb)
		
		scroll = g.ScrolledWindow()
		scroll.set_policy(g.POLICY_AUTOMATIC, g.POLICY_AUTOMATIC)
		scroll.set_shadow_type(g.SHADOW_ETCHED_IN)
		scroll.set_size_request(200, -1)
		scroll.add(self.tree_view)
		
		vbox = g.VBox(False, VSPACING)
		hbox = g.HBox(True, HSPACING)
		self.up_button = g.Button(stock = g.STOCK_GO_UP)
		self.up_button.connect("clicked", self.up_cb)
		hbox.pack_start(self.up_button)
		self.down_button = g.Button(stock = g.STOCK_GO_DOWN)
		self.down_button.connect("clicked", self.down_cb)
		hbox.pack_start(self.down_button)
		vbox.pack_end(hbox, False, False)
		self.add_button = g.Button(stock = g.STOCK_ADD)
		self.add_button.connect("button-press-event", self.add_cb)
		vbox.pack_end(self.add_button, False, False)
		self.edit_button = g.Button(stock = g.STOCK_EDIT)
		self.edit_button.connect("clicked", self.edit_cb)
		vbox.pack_end(self.edit_button, False, False)
		self.del_button = g.Button(stock = g.STOCK_DELETE)
		self.del_button.connect("clicked", self.delete_cb)
		vbox.pack_end(self.del_button, False, False)
		
		hbox = g.HBox()
		hbox.pack_start(scroll, padding = HSPACING)
		hbox.pack_start(vbox, False, True, padding = HSPACING)
		frame.add(hbox)
		
		self.update_buttons()
		
		self.tree_view.connect("cursor-changed", self.update_buttons)
		self.tree_view.connect("move-cursor", self.update_buttons)
		
	def update_buttons(self, *args):
		sel = self.tree_view.get_selection()
		has_sel = sel != None and sel.get_selected()[1] != None
		n_items = self.list_store.iter_n_children(None)
		moren1 = has_sel and n_items > 1
		self.edit_button.set_sensitive(has_sel)
		self.del_button.set_sensitive(moren1)
		self.up_button.set_sensitive(moren1 and \
			not sel.iter_is_selected(self.list_store.get_iter_first()))
		self.down_button.set_sensitive(moren1 and \
			self.list_store.iter_next(sel.get_selected()[1]) != None)
	
	def get_selected_iter(self):
		return self.tree_view.get_selection().get_selected()[1]
	
	def run_edit_dialog(self, iter):
		desc = self.list_store.get_value(iter, 1)
		edit_dialog = plugins.dialog_factory(self, False, desc,
			self.client.get_bar_widget(desc.num),
			self.client.get_label_widget(desc.num))
		edit_dialog.run_apply_destroy()
		self.update_buttons()
		
	def update_bar_name(self, desc):
		u = [None]
		def find_desc(m, p, i, u):
			if self.list_store.get_value(i, 1) == desc:
				u[0] = i
		self.list_store.foreach(find_desc, u)
		self.list_store.set(u[0], 0, desc.get_name())
	
	def font_set_cb(self, fw):
		f = fw.get_font_name()
		settings.set_label_font(f)
		self.client.update_label_font(f)
			
	
	#def label_colour_set_cb(self, cw):
	#	settings.set_label_colour(cw.get_color())
	
	def edit_cb(self, widget):
		iter = self.get_selected_iter()
		self.run_edit_dialog(iter)
	
	def row_activated_cb(self, view, path, column):
		iter = self.list_store.get_iter(path)
		self.run_edit_dialog(iter)
	
	def add_cb(self, widget, event):
		if event.button != 1:
			return True
		menu = g.Menu()
		for t, n in plugins.types.items():
			i = g.MenuItem(n)
			i.connect("activate", self.add_menu_cb, t)
			menu.append(i)
		menu.show_all()
		menu.popup(None, None, None, event.button, event.time)
		return True
	
	def add_menu_cb(self, widget, type):
		num = self.list_store.iter_n_children(None)
		desc = plugins.desc_factory(num, type)
		add_dialog = plugins.dialog_factory(self, True, desc)
		if add_dialog.run_apply_destroy() == g.RESPONSE_OK:
			iter = self.list_store.append()
			self.list_store.set(iter, 0, desc.get_name(), 1, desc)
			settings.set_num_bars(num + 1)
			plugins.all_descs.append(desc)
			self.client.add_bar(num)
		self.update_buttons()

	def fix_nums_after_delete(self, model, path, iter, data):
		if data[1] >= data[0]:
			desc = model.get_value(iter, 1)
			desc.change_num(data[1])
		data[1] += 1
		
	def delete_cb(self, widget):
		iter = self.get_selected_iter()
		desc = self.list_store.get_value(iter, 1)
		num = desc.num
		self.list_store.remove(iter)
		del plugins.all_descs[num]
		self.list_store.foreach(self.fix_nums_after_delete, [num, 0])
		settings.set_num_bars(self.list_store.iter_n_children(None))
		self.client.repack()
		self.update_buttons()
	
	def up_cb(self, widget):
		i2 = self.get_selected_iter()
		d2 = self.list_store.get_value(i2, 1)
		i1 = self.list_store.get_iter_first()
		while i1:
			next = self.list_store.iter_next(i1)
			dn = self.list_store.get_value(next, 1)
			if dn == d2:
				break
			i1 = next
		if i1:
			self.swap(i1, i2)
			
	def down_cb(self, widget):
		i1 = self.get_selected_iter()
		i2 = self.list_store.iter_next(i1)
		self.swap(i1, i2)
			
	def swap(self, i1, i2):
		d1 = self.list_store.get_value(i1, 1)
		d2 = self.list_store.get_value(i2, 1)
		n1 = d1.num
		n2 = d2.num
		self.list_store.swap(i1, i2)
		plugins.all_descs[n1] = d2
		plugins.all_descs[n2] = d1
		d1.change_num(n2)
		d2.change_num(n1)
		self.client.repack()
		self.update_buttons()
		
		

def run_dialog(client, parent = None):
	if parent and not isinstance(parent, g.Window):
		parent = None
	d = Dialog(client, parent)
	d.show_all()
	d.run()
	d.destroy()
	