import time

class ProcStat:
	def __init__(self):
		self.last_queried = -1
		self.previous_busy = None
		self.previous_idle = None
		self.get_readings(time.time())
	
	def get_readings(self, stamp):
		self.stamp = stamp
		fp = open('/proc/stat', 'r')
		self.cpus_busy = [0]
		self.cpus_idle = [0]
		while True:
			line = fp.readline()
			if not line:
				break
			if line.startswith('cpu'):
				figures = line.split()
				cpu = figures[0]
				figures = map(lambda x: long(x), figures[1:])
				busy = figures[0] + figures[1] + figures[2]
				idle = figures[3]
				if len(figures) >= 7:
					idle += figures[4]
					busy += figures[5] + figures[6]
				if len(figures) >= 8:
					busy += figures[7]
				if cpu == 'cpu':
					cpu = 0
				else:
					cpu = int(cpu[3:]) + 1
				while len(self.cpus_busy) <= cpu:
					self.cpus_busy.append(0)
					self.cpus_idle.append(0)
				self.cpus_busy[cpu] = busy
				self.cpus_idle[cpu] = idle
		fp.close()
	
	def get_cpu_load(self, num):
		t = time.time()
		if not self.previous_busy or num == self.last_queried \
			or t - self.stamp > 0.1:
			self.previous_busy = self.cpus_busy
			self.previous_idle = self.cpus_idle
			self.previous_stamp = self.stamp
			self.get_readings(t)
			self.last_queried = num
		if self.previous_busy:
			busy = self.cpus_busy[num] - self.previous_busy[num]
		else:
			busy = self.cpus_busy[num]
		if self.previous_idle:
			idle = self.cpus_idle[num] - self.previous_idle[num]
		else:
			idle = self.cpus_idle[num]
		if busy + idle == 0:
			return 0
		return long(65536 * float(busy) / (idle + busy))


class ProcMemInfo:
	def __init__(self):
		self.get_readings(time.time())
		
	def get_readings(self, stamp):
		""" Returns (used, total) """
		self.stamp = stamp
		total = 0
		used = 0
		free = 0
		buffers = 0
		cached = 0
		slab = 0
		swap_total= 0
		swap_free = 0
		free_format = False
		fp = open('/proc/meminfo', 'r')
		while True:
			line = fp.readline()
			if not line:
				break
			if line.startswith('Mem:'):
				free_format = True
				data = line.split()
				total = int(data[1])
				used = int(data[2])
				free = int(data[3])
				buffers = int(data[4])
				cached = int(data[5])
			elif line.startswith('Swap:'):
				data = line.split()
				swap_total = int(data[1])
				swap_free = int(data[3])
			elif line.startswith('SwapTotal:'):
				swap_total = int(line.split()[1])
			elif line.startswith('SwapFree:'):
				swap_free = int(line.split()[1])
			elif line.startswith('MemTotal:'):
				total = int(line.split()[1])
			elif line.startswith('MemFree:'):
				free = int(line.split()[1])
			elif line.startswith('Buffers:'):
				buffers = int(line.split()[1])
			elif line.startswith('Cached:'):
				cached = int(line.split()[1])
			elif line.startswith('Slab:'):
				slab = int(line.split()[1])
		fp.close()
		if not free_format:
			used = total - free
		self.used_mem = used - buffers - cached - slab
		self.total_mem = total
		self.used_swap = swap_total - swap_free
		self.total_swap = swap_total
				
	def get_mem_used(self):
		t = time.time()
		if t - self.stamp > 0.1:
			self.get_readings(t)
		return self.used_mem
	
	def get_mem_total(self):
		# Won't change on fly so no need to update readings
		return self.total_mem

	def get_swap_used(self):
		t = time.time()
		if t - self.stamp > 0.1:
			self.get_readings(t)
		return self.used_swap
	
	def get_swap_total(self):
		t = time.time()
		if t - self.stamp > 0.1:
			self.get_readings(t)
		return self.total_swap


class MeasuredRate:
	""" Base class for measuring rate using time between readings. Maximum
	rate may vary. """
	def __init__(self, fixed_highest = None):
		self.fixed_highest = 1
		self.highest = {}
		self.stamp = None
		self.latest_readings = {}
		self.stamp = time.time()
		self.get_readings()
	
	def pre_get_readings(self, stamp):
		self.previous_stamp = self.stamp
		self.stamp = stamp
		self.previous_readings = self.latest_readings
		self.latest_readings = {}
	
	def get_reading(self, d):
		t = time.time()
		if t - self.stamp > 0.1:
			self.pre_get_readings(t)
			self.get_readings()
		if not self.previous_readings:
			return 0
		period = self.stamp - self.previous_stamp
		if not period:
			return 0
		reading = self.calculate_reading(d, period)
		self.set_throughput(d, reading)
		return reading
	
	def get_range(self, d):
		return (0, self.highest.get(d, self.fixed_highest))
	
	def set_throughput(self, d, t):
		if t > self.get_range(d)[1]:
			self.highest[d] = t
		
	def list_devices(self):
		devices = self.latest_readings.keys()
		devices.sort()
		return devices
	



class ProcDiskStats(MeasuredRate):
	def __init__(self):
		MeasuredRate.__init__(self, 1000)
	
	def get_readings(self):
		fp = open('/proc/diskstats')
		while True:
			line = fp.readline()
			if not line:
				break
			stats = line.split()
			d = stats[2]
			stats = map(lambda x: int(x), stats[3:])
			if len(stats) > 4:
				self.latest_readings[d] = stats[2] + stats[6]
			else:
				self.latest_readings[d] = stats[1] + stats[3]
		fp.close()
	
	def calculate_reading(self, d, period):
		return float(self.latest_readings[d] - self.previous_readings[d]) / \
			period
	


class ProcNetDev(MeasuredRate):
	""" Readings here are tuple (read, write) """
	def __init__(self):
		MeasuredRate.__init__(self, 100 * 1024 * 1024 / 8)
	
	def get_readings(self):
		fp = open('/proc/net/dev')
		while True:
			line = fp.readline()
			if not line:
				break
			stats = line.split()
			if not ':' in stats[0]:
				continue
			d = stats[0].rsplit(':', 1)
			if d[1]:
				rx = int(d[1])
				tx = int(stats[8])
			else:
				rx = int(stats[1])
				tx = int(stats[9])
			self.latest_readings[d[0]] = (rx, tx)
		fp.close()
	
	def calculate_reading(self, d, period):
		return ( \
			float(self.latest_readings[d][0] - self.previous_readings[d][0]) / \
			period,
			float(self.latest_readings[d][1] - self.previous_readings[d][1]) / \
			period)
	
	def get_range(self, d):
		highest = self.highest.get(d)
		if not highest:
			highest = [self.fixed_highest, self.fixed_highest]
		return (0, highest)
	
	def set_throughput(self, d, t):
		th = self.highest.get(d)
		if not th:
			self.highest[d] = self.get_range(d)[1]
		if t[0] > self.get_range(d)[1][0]:
			self.highest[d][0] = t[0]
		if t[1] > self.get_range(d)[1][1]:
			self.highest[d][1] = t[1]
		


proc_stat = ProcStat()
proc_meminfo = ProcMemInfo()
proc_diskstats = ProcDiskStats()
proc_netdev = ProcNetDev()
				

""" The following functions are the public API. """
				

def get_cpu_load_range():
	return (0, 65536)

def get_cpu_load(num):
	return proc_stat.get_cpu_load(num + 1)



def get_mem_used():
	return proc_meminfo.get_mem_used()
	
def get_mem_range():
	return (0, proc_meminfo.get_mem_total())


def get_swap_used():
	return proc_meminfo.get_swap_used()
	
def get_swap_range():
	return (0, proc_meminfo.get_swap_total())


def get_disk_activity(d):
	return proc_diskstats.get_reading(d)

def get_disk_activity_range(d):
	return proc_diskstats.get_range(d)

def list_disks():
	return proc_diskstats.list_devices()

def init_disk_throughput(d, throughput):
	proc_diskstats.set_throughput(d, throughput)


def get_net_activity(d):
	return proc_netdev.get_reading(d)

def get_net_activity_range(d):
	return proc_netdev.get_range(d)

def list_net_devices():
	return proc_netdev.list_devices()

def init_net_throughput(d, throughput):
	proc_netdev.set_throughput(d, throughput)



