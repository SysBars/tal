import os
import sys

from choices import settings
import report

types = {}

_modules = {}

def get_platform():
	p = sys.platform
	return __import__('platform.%s' % p, globals(), locals(), [p])


# Import everything in plugins dir
dir = os.path.dirname(__file__)
for f in os.listdir(dir):
	if not f.endswith('.py') or f[0] == '_':
		continue
	f = f[:-3]
	_modules[f] = __import__('plugins.' + f, globals(), locals(), [f])

def desc_factory(num, btype = None):
	if btype == None:
		btype = settings.get_bar_type(num)
	return _modules[btype].Description(num)

def dialog_factory(parent_win, add, desc, bar_w = None, label_w = None):
	# Don't check type because if it isn't valid we've got a serious
	# bug and an exception is appropriate
	return _modules[desc.type].Dialog(parent_win, add, desc, bar_w, label_w)

def get_plugin_long_name(module_name):
	return _modules[module_name].get_long_name(module_name)


all_descs = []

for n in range(settings.get_num_bars()):
	bar = desc_factory(n)
	if bar:
		all_descs.append(bar)

