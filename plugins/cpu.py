import os

from rox import g

import addedit
from addedit import VSPACING, HSPACING
import bardesc
from choices import settings
import plugins

plugins.types['cpu'] =  _("CPU Load")


platform = plugins.get_platform()


def get_n_cpus():
	try:
		return os.sysconf("SC_NPROCESSORS_ONLN")
	except:
		raise
		# FIXME: SC_NPROCESSORS_ONLN may not be standard, try /proc/stat
		return 1


class Description(bardesc.Description):
	def __init__(self, num):
		bardesc.Description.__init__(self, num)
		self.type = 'cpu'
		self.cpu = settings.get_bar_int(num, 'cpu', -1)
	
	def get_name(self):
		if self.cpu == -1:
			return _("All CPUs")
		else:
			return _("CPU %d") % self.cpu
	
	def change_num(self, num):
		bardesc.Description.change_num(self, num)
		settings.set_bar(num, 'cpu', self.cpu)
	
	def get_range(self):
		return platform.get_cpu_load_range()
	
	def get_reading(self):
		return platform.get_cpu_load(self.cpu)
	


class Dialog(addedit.Dialog):
	def get_type_name(self):
		return _("CPU Load")
		
	def layout_for_type(self):
		hbox = g.HBox(False)
		hbox.pack_start(self.make_label(_("CPU")) , False, True, HSPACING)
		self.cpu_w = g.combo_box_new_text()
		self.cpu_w.append_text(_("All"))
		for n in range(get_n_cpus()):
			self.cpu_w.append_text(str(n))
		self.cpu_w.set_active(self.desc.cpu + 1)
		hbox.pack_start(self.cpu_w, True, True, HSPACING)
		self.vbox.pack_start(hbox, True, True, VSPACING)
		if self.instant_apply:
			self.cpu_w.connect("changed", self.cpu_changed_cb)
	
	def apply(self):
		self.cpu_changed_cb()
		addedit.Dialog.apply(self)
	
	def cpu_changed_cb(self, w = None):
		cpu = self.cpu_w.get_active()
		if cpu < 0:
			cpu = -1
		else:
			cpu -= 1
		self.desc.cpu = cpu
		settings.set_bar(self.desc.num, 'cpu', cpu)
		if self.instant_apply:
			self.bar_w.whole_redraw()
			self.parent_win.update_bar_name(self.desc)
		
	def get_default_label(self):
		return _("C")
			