import os

from rox import g

import addedit
from addedit import VSPACING, HSPACING
import bardesc
from choices import settings
import plugins

plugins.types['diskactivity'] =  _("Disk Activity")


platform = plugins.get_platform()


class Description(bardesc.Description):
	def __init__(self, num):
		bardesc.Description.__init__(self, num)
		self.type = 'diskactivity'
		self.disk = settings.get_bar(num, 'disk', 'sda')
		self.throughput = settings.get_throughput(self.disk)
		platform.init_disk_throughput(self.disk, self.throughput)
	
	def get_name(self):
		return _("Disk %s") % self.disk
	
	def change_num(self, num):
		bardesc.Description.change_num(self, num)
		settings.set_bar(num, 'disk', self.disk)
	
	def get_range(self):
		rng = platform.get_disk_activity_range(self.disk)
		if rng[1] > self.throughput:
			self.throughput = rng[1]
			settings.set_throughput(self.disk, self.throughput)
		return rng
	
	def get_reading(self):
		return platform.get_disk_activity(self.disk)
	


class Dialog(addedit.Dialog):
	def get_type_name(self):
		return _("Disk Activity")
		
	def layout_for_type(self):
		hbox = g.HBox(False)
		hbox.pack_start(self.make_label(_("Disk")) , False, True, HSPACING)
		self.disk_w = g.combo_box_new_text()
		disks = platform.list_disks()
		self.disks = disks
		for d in disks:
			self.disk_w.append_text(d)
		if self.desc.disk in disks:
			self.disk_w.set_active(disks.index(self.desc.disk))
		else:
			self.disk_w.set_active(0)
		hbox.pack_start(self.disk_w, True, True, HSPACING)
		self.vbox.pack_start(hbox, True, True, VSPACING)
		if self.instant_apply:
			self.disk_w.connect("changed", self.disk_changed_cb)
	
	def apply(self):
		self.disk_changed_cb()
		addedit.Dialog.apply(self)
	
	def disk_changed_cb(self, w = None):
		disk = self.disks[self.disk_w.get_active()]
		self.desc.disk = disk
		settings.set_bar(self.desc.num, 'disk', disk)
		if self.instant_apply:
			self.bar_w.whole_redraw()
			self.parent_win.update_bar_name(self.desc)
		
	def get_default_label(self):
		return _("D")
			