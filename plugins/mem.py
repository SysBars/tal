import addedit
import bardesc
import plugins

plugins.types['mem'] =  _("Memory")


platform = plugins.get_platform()


class Description(bardesc.Description):
	def __init__(self, num):
		bardesc.Description.__init__(self, num)
		self.type = 'mem'
		
	def get_name(self):
		return "Memory"
	
	def get_range(self):
		return platform.get_mem_range()
	
	def get_reading(self):
		return platform.get_mem_used()
	


class Dialog(addedit.Dialog):
	def get_type_name(self):
		return _("Memory")
	
	def get_default_label(self):
		return _("M")
		