import os

from rox import g

import addedit
from addedit import VSPACING, HSPACING
import bardesc
from choices import settings
import plugins

plugins.types['netactivity'] =  _("Network Activity")


platform = plugins.get_platform()

RO = 1
WO = 2
RW = 3

class Description(bardesc.Description):
	def __init__(self, num):
		bardesc.Description.__init__(self, num)
		self.type = 'netactivity'
		self.dev = settings.get_bar(num, 'dev', 'eth0')
		self.throughputs = [settings.get_throughput(self.dev + '/r'),
			settings.get_throughput(self.dev + '/w')]
		self.set_mode(settings.get_bar_int(num, 'rwmode', 3))
		platform.init_net_throughput(self.dev, self.throughputs)
	
	def set_mode(self, mode):
		self.mode = mode
		throughput = self.throughputs
		if mode == RO:
			self.throughput = throughput[0]
		elif mode == WO:
			self.throughput = throughput[1]
		elif mode == RW:
			self.throughput = throughput[0] + throughput[1]	
			
	def get_name(self):
		if self.mode == RO:
			mode = _("r")
		elif self.mode == WO:
			mode = _("s")
		elif self.mode == RW:
			mode = _("r+s")
		return _("Net adaptor %s (%s)") % (self.dev, mode)
	
	def change_num(self, num):
		bardesc.Description.change_num(self, num)
		settings.set_bar(num, 'dev', self.dev)
		settings.set_bar(num, 'mode', self.mode)
	
	def get_range(self):
		drng = platform.get_net_activity_range(self.dev)
		if self.mode == RO:
			rng = (drng[0], drng[1][0])
		elif self.mode == WO:
			rng = (drng[0], drng[1][1])
		elif self.mode == RW:
			rng = (drng[0], drng[1][0] + drng[1][1])
		if rng[1] > self.throughput:
			self.throughput = rng[1]
		if drng[1][0] > self.throughputs[0]:
			self.throughputs[0] = drng[1][0]
			settings.set_throughput(self.dev + '/r', drng[1][0])
		if drng[1][1] > self.throughputs[1]:
			self.throughputs[1] = drng[1][1]
			settings.set_throughput(self.dev + '/w', drng[1][1])
		return rng
	
	def get_reading(self):
		readings = platform.get_net_activity(self.dev)
		if self.mode == RO:
			return readings[0]
		elif self.mode == WO:
			return readings[1]
		elif self.mode == RW:
			return readings[0] + readings[1]
	


class Dialog(addedit.Dialog):
	def get_type_name(self):
		return _("Network Activity")
		
	def layout_for_type(self):
		hbox = g.HBox(False)
		hbox.pack_start(self.make_label(_("Device")) , False, True, HSPACING)
		self.dev_w = g.combo_box_new_text()
		devs = platform.list_net_devices()
		self.devs = devs
		for d in devs:
			self.dev_w.append_text(d)
		if self.desc.dev in devs:
			self.dev_w.set_active(devs.index(self.desc.dev))
		else:
			self.dev_w.set_active(0)
		hbox.pack_start(self.dev_w, True, True, HSPACING)
		self.vbox.pack_start(hbox, True, True, VSPACING)
		
		hbox = g.HBox(True)
		self.ro_w = g.RadioButton(None, _("Received"))
		self.wo_w = g.RadioButton(self.ro_w, _("Sent"))
		self.rw_w = g.RadioButton(self.ro_w, _("Both"))
		hbox.pack_start(self.ro_w, True, True, HSPACING)
		hbox.pack_start(self.wo_w, True, True, HSPACING)
		hbox.pack_start(self.rw_w, True, True, HSPACING)
		if self.desc.mode == RO:
			self.ro_w.set_active(True)
		elif self.desc.mode == WO:
			self.wo_w.set_active(True)
		elif self.desc.mode == RW:
			self.rw_w.set_active(True)
		self.vbox.pack_start(hbox, True, True, VSPACING)
		
		if self.instant_apply:
			self.dev_w.connect("changed", self.dev_changed_cb)
			self.ro_w.connect("toggled", self.mode_toggled_cb, RO)
			self.wo_w.connect("toggled", self.mode_toggled_cb, WO)
			self.rw_w.connect("toggled", self.mode_toggled_cb, RW)
	
	def apply(self):
		self.dev_changed_cb()
		addedit.Dialog.apply(self)
		for radio, mode in ((self.ro_w, RO), (self.wo_w, WO), (self.rw_w, RW)):
			if radio.get_active():
				self.set_mode(mode)
				break
	
	def dev_changed_cb(self, w = None):
		dev = self.devs[self.dev_w.get_active()]
		self.desc.dev = dev
		settings.set_bar(self.desc.num, 'dev', dev)
		if self.instant_apply:
			self.bar_w.whole_redraw()
			self.parent_win.update_bar_name(self.desc)
	
	def mode_toggled_cb(self, radio, mode):
		if radio.get_active():
			self.set_mode(mode)
			self.parent_win.update_bar_name(self.desc)
	
	def set_mode(self, mode):
		settings.set_bar(self.desc.num, 'rwmode', mode)
		self.desc.set_mode(mode)
		
	def get_default_label(self):
		return _("N")
			