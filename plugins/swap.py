import addedit
import bardesc
import plugins

plugins.types['swap'] =  _("Swap")


platform = plugins.get_platform()


class Description(bardesc.Description):
	def __init__(self, num):
		bardesc.Description.__init__(self, num)
		self.type = 'swap'
		
	def get_name(self):
		return "Swap"
	
	def get_range(self):
		return platform.get_swap_range()
	
	def get_reading(self):
		return platform.get_swap_used()
	


class Dialog(addedit.Dialog):
	def get_type_name(self):
		return _("Swap")
	
	def get_default_label(self):
		return _("S")
	