#!/usr/bin/env/python

import time

old_idle = [0, 0, 0]
old_busy = [0, 0, 0]

fp = open('/proc/stat', 'r')
while True:
	while True:
		idle = [0, 0, 0]
		busy = [0, 0, 0]
		line = fp.readline()
		if not line:
			break
		if line.startswith('cpu'):
			figures = line.split()
			cpu = figures[0]
			figures = map((lambda x: long(x)), figures[1:])
			busy = figures[0] + figures[1] + figures[2]
			idle = figures[3]
			if len(figures) >= 7:
				idle += figures[4]
				busy += figures[5] + figures[6]
			if len(figures) >= 8:
				busy += figures[7]
			if cpu == 'cpu':
				cpu = 0
			else:
				cpu = int(cpu[3:]) + 1
			idle[cpu] = idle
			busy[cpu] = busy
	for n in range(3):
		if n == 0:
			print "CPU A",
		else:
			print "CPU", n - 1,
		print busy[cpu], ':', idle[cpu] + busy[cpu], ' = ', 100 * busy[cpu] / (busy[cpu] + idle[cpu]), '%'
		old_idle = idle
		old_busy = busy
	time.sleep(1)
	fp.rewind()