from rox import g

def _run_message_dialog(type, title, parent_win, message):
	d = g.MessageDialog(parent_win,
		g.DIALOG_MODAL | g.DIALOG_DESTROY_WITH_PARENT,
		type, g.BUTTONS_CLOSE, message)
	if title:
		d.set_title(title)
	response = d.run()
	d.destroy()
	return response

def warning(parent_win, message):
	_run_message_dialog(g.MESSAGE_WARNING, _("Warning from SysBars"),
		parent_win, message)
