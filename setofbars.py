import os

import pango

import rox
from rox import g
import rox.AppInfo

import barwidget
from choices import settings
import choicesui
import plugins

BGAP = 1	# Between each bar
LGAP = 2	# Between label and bar

class SetOfBars(g.EventBox):
	def __init__(self, orient = barwidget.VERT, length = -1,
		menu_pos_func = None):
		g.EventBox.__init__(self)
		self.menu_pos_func = menu_pos_func
		self.layout(orient, length)
		self.connect("button-press-event", self.button_cb)
		self.connect("button-release-event", self.button_cb)
	
	def layout(self, orient, length = -1):
		self.orient = orient
		self.length = length
		if orient == barwidget.VERT:
			self.box = g.HBox(False, BGAP)
			self.SubBox = g.VBox
			self.pack = g.VBox.pack_end
		else:
			self.box = g.VBox(False, BGAP)
			self.SubBox = g.HBox
			self.pack = g.HBox.pack_start
		
		self.labels = []
		self.bars = []
		
		for n in range(settings.get_num_bars()):
			self.add_bar(n)
					
		self.add(self.box)
		self.box.show()
	
	def repack(self, orient = None, length = None):
		if orient == None:
			orient = self.orient
		if length == None:
			length = self.length
		self.box.destroy()
		self.layout(orient, length)
	
	def update_label_font(self, font_name = None):
		if not font_name:
			font_name = settings.get_label_font()
		pfd = pango.FontDescription(font_name)
		for l in self.labels:
			l.modify_font(pfd)
	
	def add_bar(self, num):
		desc = plugins.all_descs[num]
		sub_box = self.SubBox(False, LGAP)
		l = g.Label(desc.label)
		pfd = pango.FontDescription(settings.get_label_font())
		l.modify_font(pfd)
		self.pack(sub_box, l, False, True, 0)
		self.labels.append(l)
		b = barwidget.BarWidget(desc, self.orient, self.length)
		self.pack(sub_box, b, True, True, 0)
		self.bars.append(b)
		self.box.add(sub_box)
		sub_box.show_all()
	
	def button_cb(self, widget, event):
		if event.type == g.gdk.BUTTON_RELEASE and event.button == 1:
			choicesui.run_dialog(self)
		elif event.type == g.gdk.BUTTON_PRESS and event.button == 3:
			menu = g.Menu()
			i = g.ImageMenuItem(g.STOCK_ABOUT)
			i.connect("activate", show_about)
			menu.append(i)
			i = g.ImageMenuItem(g.STOCK_PREFERENCES)
			i.connect("activate", self.run_choices_dialog)
			menu.append(i)
			i = g.ImageMenuItem(g.STOCK_QUIT)
			i.connect("activate", quit)
			menu.append(i)
			menu.show_all()
			menu.popup(None, None, self.menu_pos_func, event.button, event.time)
	
	def run_choices_dialog(self, item):
		choicesui.run_dialog(self)
	
	def get_bar_widget(self, num):
		return self.bars[num]
	
	def get_label_widget(self, num):
		return self.labels[num]
	

logo = None
authors = None
version = None
			

def show_about(item):
	global logo, authors, version
	if not authors and not version:
		app_info = rox.AppInfo.AppInfo(os.path.join(rox.app_dir, 'AppInfo.xml'))
		authors = app_info.getAuthors()
		version = \
			app_info.findElements('Version')[0].firstChild.nodeValue.split()[0]
	if not authors:
		authors = "Tony Houghton"
	if not logo:
		logo = g.gdk.pixbuf_new_from_file_at_size( \
			os.path.join(rox.app_dir, '.DirIcon'), 64, 64)
	d = g.AboutDialog()
	d.set_name("SysBars")
	if version:
		d.set_version(version)
	d.set_copyright("\302\251 Tony Houghton")
	d.set_website('http://www.realh.co.uk')
	d.set_authors([authors])
	d.set_logo(logo)
	d.run()
	d.destroy()


def quit(item):
	rox.toplevel_unref()
	g.main_quit()

			
		